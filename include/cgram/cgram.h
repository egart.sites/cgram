/*
 *This file is part of the cGram distribution (https://github.com/h3xcode/cgram).
 *Copyright (c) 2020 DirectName (h3xcode).
 *
 *This program is free software: you can redistribute it and/or modify  
 *it under the terms of the GNU General Public License as published by  
 *the Free Software Foundation, version 3.
 *
 *This program is distributed in the hope that it will be useful, but 
 *WITHOUT ANY WARRANTY; without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 *General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License 
 *along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CGRAM_H
#define CGRAM_H

#define URL "https://api.telegram.org/bot%s/%s"
#define CGRAM_VERSION "v0.1"

#include <cjson/cJSON.h>
#include <curl/curl.h>
#include <stdbool.h>

#include "types.h"

typedef enum {MarkdownV2, HTML, NONE} parseMode;

typedef struct{
    bool ok;
    int error_code;
    char *description;
} cgram_status;

typedef struct{
    char *TOKEN;
    void (*messageListener)();
    void (*commandListener)();
    bool initialized;
    int updates_offset;
} cBot;

// cGram
void init_cgram(cBot *bot, char *TOKEN);
void cgram_setMessageListener(cBot *bot, void *function);
void cgram_setCommandListener(cBot *bot, void *function);
void cgram_startPolling(cBot *bot, int sleep_time);

// Telegram bot API
cgram_User_t cgram_getMe(cBot *bot);
cgram_Message_t cgram_sendMessage(cBot *bot, long chat_id, char text[]);
cgram_Message_t cgram_replyToMessage(cBot *bot, long chat_id, long message_id, char text[]);
cgram_Message_t cgram_fsendMessage(cBot *bot, long chat_id, char text[], parseMode parse_mode);
cgram_Message_t cgram_freplyToMessage(cBot *bot, long chat_id, long message_id, char text[], parseMode parse_mode);

// Parsers
char *cgram_parse(const char *ret, const char *object);
cgram_Message_t cgram_getReplyMessage(cgram_Message_t message);

#endif
