#ifndef CGRAM_STICKERS_H
#define CGRAM_STICKERS_H

typedef struct{
    char *point;
    float x_shift;
    float y_shift;
    float scale;
} cgram_MaskPosition_t;

typedef struct{
    char *file_id;
    char *file_unique_id;
    int width;
    int height;
    bool is_animated;
    cgram_PhotoSize_t thumb;
    char *emoji;
    char *set_name;
    cgram_MaskPosition_t mask_position;
    int file_size;
} cgram_Sticker_t;

typedef struct{
    char *name;
    char *title;
    bool is_animated;
    bool contains_masks;
    cgram_Sticker_t *stickers;
    cgram_PhotoSize_t thumb;
} cgram_StickerSet_t

#endif
