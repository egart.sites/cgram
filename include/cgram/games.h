#ifndef CGRAM_GAMES_H
#define CGRAM_GAMES_H

typedef struct{
    char *title;
    char *description;
    cgram_PhotoSize_t *photo;
    char *text;
    cgram_MessageEntity_t *text_entities;
    cgram_Animation_t animation;
} cgram_Game_t;

typedef struct{
    int position;
    cgram_User_t user;
    int score;
} cgram_GameHighScore_t;

#endif
